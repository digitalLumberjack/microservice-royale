Microservice Royale
===================
https://digitallumberjack.gitlab.io/microservice-royale/


Microservice Royale shows, by metrics, the difference between several micro service oriented servers, written in different languages and different frameworks.


Its goal is not to decide what is the best language/framework to create a microservice, but to provide specific metrics that can help you decide what is the language you want to build the service **you need**.


For example, if you need a minimal memory usage, or a really quick boot time, you can validate your tech choices by using Microservice Royale. 


You can access results of the last build on [https://digitallumberjack.gitlab.io/microservice-royale/](https://digitallumberjack.gitlab.io/microservice-royale/)

We compare identical features in different languages/frameworks:
* [X] go + net/http
* [X] nodejs
* [X] python
* [X] java 
* [X] java9 
* [X] java + springboot
* [X] java + vert.x
* [X] kotlin + micronaut
* [X] ktor
* [ ] nodejs + express
* [ ] scala + play framework
* [X] scala + akka
* [ ] rust
* [X] rust-iron
* [X] rust-rocket
* [X] rust-hyper
* [ ] erlang
* [ ] elixir
* [X] php apache
* [X] nginx raw


The server must be able to:
* [X] serve a /healthcheck
* [ ] create a system process and return the result of this process
* [ ] convert an image from jpg to png


The metrics must:
* provide : 
  * [X] a way to limit cpu for containers
  * [X] a one command line test for all listed containers
* returns : 
  * [X] a json with following metrics
  * [X] the server startup time
  * [X] the server startup memory usage
  * [X] the server mean response time for a defined amount of request
  * [ ] the size of the image
  * [ ] time between image startup command to an orchestrator and server up

### Kitano

![kitano-banner](/uploads/497de010941fce35bc0371de655344d1/kitano-banner.png)

Kitano is used to start tests on a list of images.
Using docker golang api, it downloads each image, start a container, and create a json with metrics computed.
 
```bash
Usage of ./kitano:
  -charge uint
    	Number of request to send when load testing (default 1000)
  -healthcheckEndpoint string
    	Healthcheck endpoint to contact (default "/healthcheck")
  -healthcheckExpected string
    	Expected healthcheck endpoint response (default "{\"State\":\"OK\"}")
  -images string
    	Images to test, separated by spaces
  -loadEndpoint string
    	Load test endpoint
  -loadExpected string
    	Expected load endpoint response
  -nanocpu int
    	Nanocpu limit for each container. 1000000000 = 1 core
  -port uint
    	Exposed port of the images (if not set, determined from EXPOSE on image) (default 0)
  -pull
    	Pull images
  -timeout int
    	Timeout in second for waiting healthcheck on a service (default 600)
```
