# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Runs kitano with 0,1 cpu and a timeout of 30s
- Runs kitano with 0,1 cpu and a timeout of 30s
- Microservice java9
- Microservice java
- Microservice java springboot
- Microservice java vertx
- Microservice go
- Microservice nodejs
- Microservice python
- Microservice scala akka
- Microservice rust iron
- Microservice rust rocket
- Microservice rust hyper
- kitano tester project with docker API
- board project for reports
