package main

import (
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"github.com/docker/docker/client"
	"github.com/docker/docker/api/types"
	"context"
	"time"
	"github.com/docker/docker/api/types/container"
	"fmt"
	"github.com/jhoonb/archivex"
	"os"
	"encoding/json"
	"github.com/pkg/errors"
)

type Docker struct {
	client *client.Client
	ctx    context.Context
}

func NewDocker(client *client.Client, ctx context.Context) (*Docker) {
	return &Docker{client: client, ctx: ctx}
}

func (d *Docker) CreateContainer(image string, nanoCpu int64) (container.ContainerCreateCreatedBody, error) {
	resources := container.Resources{}
	if nanoCpu > 0 {
		resources = container.Resources{NanoCPUs: nanoCpu}
	}
	return d.client.ContainerCreate(d.ctx, &container.Config{Image: image}, &container.HostConfig{Resources: resources}, nil, "")
}

func (d *Docker) StartContainer(containerId string) error {
	return d.client.ContainerStart(d.ctx, containerId, types.ContainerStartOptions{})
}

func (d *Docker) RemoveContainer(containerId string) error {
	return d.client.ContainerRemove(d.ctx, containerId, types.ContainerRemoveOptions{Force:true})
}
func (d *Docker) GetIp(containerId string) (string, error) {
	inspect, err := d.client.ContainerInspect(d.ctx, containerId)
	if err != nil {
		return "", err
	}
	return inspect.NetworkSettings.IPAddress, nil
}

func (d *Docker) PullImage(image string) error {
	buf, err := d.client.ImagePull(d.ctx, image, types.ImagePullOptions{})
	if err != nil {
		return err
	}
	defer buf.Close()
	s, err := ioutil.ReadAll(buf)
	log.Debugf("%s", s)
	return nil
}

func (d *Docker) GetTimeToRespond(containerID string, port uint, path string, timeoutSeconds int64, expected string) (time.Duration, error) {
	inspect, err := d.client.ContainerInspect(d.ctx, containerID)
	if err != nil {
		return 0, err
	}
	if port == 0 {
		port, err = findPort(inspect)
		if err != nil {
			return 0, err
		}
	}
	log.Debugf("Waiting for http endpoint availability on IP %s and port %d", inspect.NetworkSettings.IPAddress, port)
	return WaitForResponse(inspect.NetworkSettings.IPAddress, port, path, expected, timeoutSeconds)
}

func findPort(inspect types.ContainerJSON) (uint, error) {
	log.Debugf("Using exposed port of image : %s", inspect.Image)
	for key := range inspect.Config.ExposedPorts {
		return (uint)(key.Int()), nil
	}
	return 0, errors.New(fmt.Sprintf("Unable to find exposed port on %s", inspect.Image))
}

type MeanResult struct {
	result time.Duration
	err    error
}

func meantime(ip string, port uint, path string, expected string, timeoutSeconds int64, charge uint, result chan MeanResult) {
	var i uint
	var duration time.Duration = 0

	for i = 0; i < charge; i++ {

		time, err := RequestTime(ip, port, path, expected, timeoutSeconds)
		if err != nil {
			result <- MeanResult{result: 0, err: err}
		}
		duration = duration + time
	}
	result <- MeanResult{result: duration, err: nil}
}

func (d *Docker) GetMeanTime(containerID string, port uint, path string, timeoutSeconds int64, expected string, charge uint, loadThreads uint) (time.Duration, error) {
	inspect, err := d.client.ContainerInspect(d.ctx, containerID)
	if err != nil {
		return 0, err
	}
	if port == 0 {
		port, err = findPort(inspect)
		if err != nil {
			return 0, err
		}
	}

	log.Debugf("Starting load test on IP %s and port %d", inspect.NetworkSettings.IPAddress, port)
	var routine uint
	var c = make(chan MeanResult, loadThreads)
	var duration time.Duration = 0

	for routine = 0; routine < loadThreads; routine ++ {
		go meantime(inspect.NetworkSettings.IPAddress, port, path, expected,timeoutSeconds, charge, c)
	}
	var count = loadThreads
	for result := range c {
		count = count -1
		if result.err != nil {
			return 0, result.err
		}else {
			duration = duration + result.result
		}
		if count<=0 {
			close(c)
		}
	}
	log.Debugf("Load test over on %s", inspect.NetworkSettings.IPAddress)
	return (time.Duration)((uint)(duration) / charge), nil

}

func (d *Docker) GetMemory(containerID string) (uint64, error) {
	t, err := d.client.ContainerStats(d.ctx, containerID, false)
	if err != nil {
		return 0, err
	}

	stats, err := ioutil.ReadAll(t.Body)
	var contStat types.Stats
	log.Debugf("Stats : %s\n", stats)
	if err := json.Unmarshal(stats, &contStat); err != nil {
		return 0, err
	}
	return contStat.MemoryStats.Usage, nil

}

func buildImage(cli *client.Client, path string) {
	theTar := fmt.Sprintf("%s/temp.tar", path)
	tarr := new(archivex.TarFile)
	tarr.Create(theTar)
	tarr.AddAll(path, false)
	tarr.Close()
	dockerBuildContext, err := os.Open(theTar)
	defer dockerBuildContext.Close()
	options := types.ImageBuildOptions{
		SuppressOutput: false,
		Remove:         true,
		ForceRemove:    true,
		PullParent:     true}
	buildResponse, err := cli.ImageBuild(context.Background(), dockerBuildContext, options)
	if err != nil {
		fmt.Printf("%s", err.Error())
	}
	fmt.Printf("********* %s **********", buildResponse.OSType)
	response, err := ioutil.ReadAll(buildResponse.Body)
	if err != nil {
		fmt.Printf("%s", err.Error())
	}
	fmt.Println(string(response))
}
