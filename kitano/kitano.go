package main

import (
	log "github.com/sirupsen/logrus"
	"context"
	"fmt"
	"flag"
	"strings"
	"github.com/docker/go-connections/nat"
	"github.com/docker/docker/client"
	"unicode/utf8"
	"time"
	"encoding/json"
)

type Board struct {
	Date         time.Time
	NanoCpu      int64
	Candidats    []MicroserviceResult
	LoadRequests uint
	LoadThreads  uint
}

type MicroserviceResult struct {
	ImageName         string
	State             string
	TimeToHealthcheck int64
	LoadMeanTime      int64
	MemoryUsage       uint64
}

func main() {
	log.SetLevel(log.DebugLevel)
	imagesArg := flag.String("images", "", "Images to test, separated by spaces")
	pull := flag.Bool("pull", false, "Pull images")
	nanoCpuArg := flag.Int64("nanocpu", 0, "Nanocpu limit for each container. 1000000000 = 1 core")
	timeout := flag.Int64("timeout", 600, "Timeout in second for waiting healthcheck on a service")
	healthEndpoint := flag.String("healthcheckEndpoint", "/healthcheck", "Healthcheck endpoint to contact")
	healthExpected := flag.String("healthcheckExpected", "{\"State\":\"OK\"}", "Expected healthcheck endpoint response")
	port := flag.Uint("port", 0, "Exposed port of the images (if not set, determined from EXPOSE on image)")
	loadEndpoint := flag.String("loadEndpoint", "", "Load test endpoint")
	loadExpected := flag.String("loadExpected", "", "Expected load endpoint response")
	loadThreads := flag.Uint("loadThreads", 1, "Number of threads to use for load tests")
	charge := flag.Uint("charge", 1000, "Number of request to send when load testing")

	flag.Parse()

	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	dock := NewDocker(cli, ctx)
	var results []MicroserviceResult

	if utf8.RuneCountInString(*imagesArg) > 0 {
		images := strings.Split(*imagesArg, " ")
		for _, image := range images {
			if len(image) > 0 {
				result, err := startupTest(dock, image, *pull, *nanoCpuArg, *timeout, *port, *healthEndpoint, *healthExpected, *loadEndpoint, *loadExpected, *charge, *loadThreads)
				if err != nil {
					panic(err)
					return
				}
				results = append(results, result);
			}
		}
	}
	js, err := json.Marshal(Board{Date: time.Now(), NanoCpu: *nanoCpuArg, LoadRequests: *charge, LoadThreads: *loadThreads, Candidats: results})
	if err != nil {
		panic(err)
	}
	fmt.Printf(string(js))
}

func createPortMap(port int) (nat.PortMap) {
	return nat.PortMap{
		nat.Port(fmt.Sprintf("%d/tcp", port)): []nat.PortBinding{
			{HostIP: "0.0.0.0", HostPort: fmt.Sprintf("%d", port)},
		},
	}
}

func startupTest(dock *Docker, image string, pull bool, nanoCpu int64, timeoutSeconds int64, port uint, healthEndpoint string,
	healthExpected string, loadEndpoint string, loadExcepted string, charge uint, loadThreads uint) (MicroserviceResult, error) {
	if pull {
		log.Infof("Pulling image %s", image)
		if err := dock.PullImage(image); err != nil {
			return MicroserviceResult{}, err
		}
	}

	log.Infof("Creating container for %s", image)
	cont, err := dock.CreateContainer(image, nanoCpu)
	if err != nil {
		return MicroserviceResult{}, err
	}
	log.Infof("Starting container for %s", image)
	if err := dock.StartContainer(cont.ID); err != nil {
		return MicroserviceResult{}, err
	}

	defer dock.RemoveContainer(cont.ID)

	state := "KO"

	log.Infof("Starting healthcheck for %s", image)
	hcTime, err := dock.GetTimeToRespond(cont.ID, port, healthEndpoint, timeoutSeconds, healthExpected)
	if err != nil {
		state = "KO"
		log.Infof("Unable to contact healthcheck for image %s", image)
	} else {
		state = "OK"
		log.Infof("Healthcheck for image %s responded in %f seconds", image, hcTime.Seconds())
	}
	var loadMeanTime time.Duration
	if loadEndpoint != "" {
		log.Infof("Starting load test for %s", image)
		loadMeanTime, err = dock.GetMeanTime(cont.ID, port, loadEndpoint, timeoutSeconds, loadExcepted, charge, loadThreads)
		if err != nil {
			state = "KO"
			log.Infof("Error while load test for image %s", image)
		} else {
			state = "OK"
			log.Infof("Load test for image %s success, mean time %d nanoseconds", image, loadMeanTime.Nanoseconds())
		}
	}

	log.Infof("Getting memory stack for %s", image)
	mem, err := dock.GetMemory(cont.ID)
	if err != nil {
		return MicroserviceResult{}, err
	}
	log.Infof("Memory usage for %s is %d", image, mem)

	return MicroserviceResult{ImageName: image, State: state, TimeToHealthcheck: hcTime.Nanoseconds(), MemoryUsage: mem, LoadMeanTime: loadMeanTime.Nanoseconds()}, nil
}
