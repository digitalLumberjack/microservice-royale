package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"time"
	"net/http"
	"errors"
)

func WaitForResponse(host string, port uint, path string, expected string, timeoutInSeconds int64) (time.Duration, error) {
	startup := time.Now()
	sinceStartup := time.Since(startup)
	for sinceStartup < time.Duration(timeoutInSeconds*int64(time.Second)) {
		sinceStartup = time.Now().Sub(startup)
		resp, err := http.Get(fmt.Sprintf("http://%s:%d%s", host, port, path))
		if err != nil {
			time.Sleep(10 * time.Millisecond)
			continue
		}
		body, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			time.Sleep(10 * time.Millisecond)
			continue
		}
		if strings.Compare(string(body), expected) == 0 {
			return sinceStartup, nil
		} else {
			return sinceStartup, errors.New(fmt.Sprintf("Endpoint %s:%d/%s returned unexpected response", host, port, path))
		}
	}
	return 0, errors.New(fmt.Sprintf("Unable to join endpoint %s:%d/%s", host, port, path))
}

func RequestTime(host string, port uint, path string, expected string, timeoutInSeconds int64) (time.Duration, error) {
	startup := time.Now()
	resp, err := http.Get(fmt.Sprintf("http://%s:%d%s", host, port, path))
	if err != nil {
		return 0, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return 0, err
	}
	sinceStartup := time.Since(startup)
	if strings.Compare(string(body), expected) == 0 {
		return sinceStartup, nil
	} else {
		return sinceStartup, errors.New(fmt.Sprintf("Endpoint %s:%d/%s returned unexpected response", host, port, path))
	}
}
