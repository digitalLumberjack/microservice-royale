var http = require('http');

http.createServer(function (req, res) {
  if (req.url === '/healthcheck') {
    var body = '{"State":"OK"}';
    res.writeHead(200, {'Content-Length': Buffer.byteLength(body), 'Content-Type': 'application/json'});
    res.end(body, 'utf8');
  } else {
    res.writeHead(404);
    res.end();
  }
}).listen(9000);
