package main

import (
	"fmt"
	"encoding/json"
	"net/http"
)

type HealthCheck struct {
	State string
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	healthCheck := HealthCheck{"OK"}
	js, err := json.Marshal(healthCheck)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func main() {
	http.HandleFunc("/healthcheck", healthcheck)
	fmt.Println("Serving on 9000")
	http.ListenAndServe(":9000", nil)
}