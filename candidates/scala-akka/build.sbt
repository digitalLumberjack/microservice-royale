import Dependencies._
import sbt.Keys.libraryDependencies

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "me.digitallumberjack",
      scalaVersion := "2.12.1",
      version      := "1.0.0-SNAPSHOT"
    )),
    name := "ms-royale-scala-akka",
    libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.0.9",
    assemblyJarName in assembly := "msroyale-scala-akka.jar",
    mainClass in assembly := Some("msroyale.WebServer")
  )