package msroyale;

import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {

	@GetMapping(value = "/healthcheck", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public String healthcheck() {
		String response = "{\"State\":\"OK\"}";
		return response;
	}
}
