extern crate futures;
extern crate hyper;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;

use futures::future::FutureResult;

use hyper::{Get, StatusCode};
use hyper::header::ContentType;
use hyper::server::{Http, Service, Request, Response};

#[derive(Serialize)]
struct Health {
    State: String
}

struct HealthCheck;

impl Service for HealthCheck {
    type Request = Request;
    type Response = Response;
    type Error = hyper::Error;
    type Future = FutureResult<Response, hyper::Error>;

    fn call(&self, req: Request) -> Self::Future {
        futures::future::ok(match (req.method(), req.path()) {
            (&Get, "/healthcheck") => {
                Response::new()
                    .with_header(ContentType::json())
                    .with_body(serde_json::to_string(&Health {
                        State: "OK".to_owned()
                    }).unwrap())
            },
            _ => {
                Response::new()
                    .with_status(StatusCode::NotFound)
            }
        })
    }
}

fn main() {
    let addr = "0.0.0.0:9000".parse().unwrap();
    let mut server = Http::new().bind(&addr, || Ok(HealthCheck)).unwrap();
    server.no_proto();
    println!("Listening on http://{} with 1 thread.", server.local_addr().unwrap());
    server.run().unwrap();
}
