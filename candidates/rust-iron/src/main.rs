extern crate iron;
#[macro_use] extern crate router;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;

use iron::{Iron, Request, Response, IronResult};
use iron::status;
use iron::mime::Mime;

#[derive(Serialize)]
struct Health {
    State: String
}

fn health_check(_: &mut Request) -> IronResult<Response> {
    let content_type = "application/json".parse::<Mime>().unwrap();
    let body = serde_json::to_string(&Health {
        State: "OK".to_owned()
    }).unwrap();
    Ok(Response::with((content_type, status::Ok, body)))
}

fn main() {
    let router = router!(root: get "/healthcheck" => health_check);
    Iron::new(router).http("0.0.0.0:9000").unwrap();
    println!("listening on port 9000...");
}
