package micronaut

import io.micronaut.context.ApplicationContext;

import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.on
import org.jetbrains.spek.api.dsl.it

import org.junit.jupiter.api.Assertions.*

class HealthControllerSpec: Spek({

    given("Application running") {
        var embeddedServer : EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
        var client : HttpClient = HttpClient.create(embeddedServer.url)

        on("Perform request on healthcheck endpoint") {
            val response = client.toBlocking().retrieve("/healthcheck")

            it("Should respond with ok state") {
                assertEquals(response, "{\"State\":\"OK\"}")
            }
        }

        afterGroup {
            client.close()
            embeddedServer.close()
        }
    }
})