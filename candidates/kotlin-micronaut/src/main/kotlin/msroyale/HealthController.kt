package micronaut

import io.micronaut.http.annotation.*;

@Controller("/") 
class HealthController {

    @Get("/healthcheck")
    fun healthcheck() = "{\"State\":\"OK\"}"
}