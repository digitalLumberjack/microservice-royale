package msroyale

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        val server = embeddedServer(Netty, port = 9000) {
            routing {
                get("/healthcheck") {
                    call.respondText("{\"State\":\"OK\"}", ContentType.Application.Json)
                }
            }
        }
        server.start(wait = true)
    }
}