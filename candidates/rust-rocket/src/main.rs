#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

use rocket_contrib::Json;

#[derive(Serialize)]
struct Health {
    State: String
}

#[get("/healthcheck")]
fn health_check() -> Json<Health> {
    Json(Health {
        State: "OK".to_owned()
    })
}

fn main() {
    rocket::ignite().mount("/", routes![health_check]).launch();
}